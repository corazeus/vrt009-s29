package com.zuittinfo;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 482479038240457436L;
	
	public void init() throws ServletException{
		
		System.out.println("**************************************");
		System.out.println(" Details Servlet has been initialized ");
		System.out.println("**************************************");
		
	}
	
	public void destroy() {
		
		System.out.println("************************************");
		System.out.println(" Details Servlet has been destroyed ");
		System.out.println("************************************");
		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		PrintWriter out = res.getWriter();
		
		//Firstname: gets fname variable using System Properties
		String fname = System.getProperty("fname");
		
		//Lastname: gets lastname variable using HttpSession request
		HttpSession session = req.getSession();
		String lname = session.getAttribute("lname").toString();
		
		//Contact: gets contact variable using sendRedirect from User Servlet using contact as parameter
		String contact = req.getParameter("contact");
		
		//Email: gets email variable using Servlet Context
		ServletContext srvCon = getServletContext();
		String email = srvCon.getAttribute("email").toString();
		
		//Print Details
		out.println("<h1> Welcome to Phonebook </h1>"
					+"<p> Firstname: "+fname+"</p>"
					+"<p> Lastname: "+lname+"</p>"
					+"<p> Contact: "+contact+"</p>"
					+"<p> Email: "+email+"</p>");
	
	}

}
