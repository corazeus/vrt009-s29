package com.zuittinfo;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2153528618197182119L;
	
	public void init() throws ServletException{
		
		System.out.println("***********************************");
		System.out.println(" User Servlet has been initialized ");
		System.out.println("***********************************");
		
	}
	
	public void destroy() {
		
		System.out.println("*********************************");
		System.out.println(" User Servlet has been destroyed ");
		System.out.println("*********************************");
		
	}
	
	public void doPost( HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		PrintWriter out = res.getWriter();
		
		//Firstname: forwards fname variable using System Properties
		String fname = req.getParameter("firstname");
		System.getProperties().put("fname", fname);
		
		//Lastname: forwards lastname variable using HttpSession request
		String lname = req.getParameter("lastname");
		HttpSession session = req.getSession();
		session.setAttribute("lname", lname);
		
		//Email: forwards email variable using Servlet Context
		String email = req.getParameter("email");
		ServletContext srvCon = getServletContext();
		srvCon.setAttribute("email", email);
		
		//Contact: forwards contact variable using sendRedirect using contact as parameter
		String contact = req.getParameter("contact");

		//Redirect to Details Servlet
		res.sendRedirect("details?contact="+contact);
		
	}

}
